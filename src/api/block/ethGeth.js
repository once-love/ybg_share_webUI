import request from '@/utils/request'

/**
 * 获取节点信息
 *
 * @returns {AxiosPromise}
 */
export function gethInfo() {
  return request({
    url: '/api/shareadmin/eth/gethInfo',
    method: 'post'
  })
}

/**
 * 开启挖矿
 * @returns {AxiosPromise}
 */
export function minerStart() {
  return request({
    url: '/api/shareadmin/eth/minerStart',
    method: 'post'
  })
}

/**
 * 停止挖矿
 * @returns {AxiosPromise}
 */
export function minerStop() {
  return request({
    url: '/api/shareadmin/eth/minerStop',
    method: 'post'
  })
}
