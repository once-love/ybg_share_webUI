import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  console.log('进来src 目录下的permission.js')
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)
  const hadLoadRoute = sessionStorage.getItem('hadLoadRoute')
  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    // console.log('permission.js  ---25')
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
    //  console.log('permission.js  ---31')
      // determine whether the user has obtained his permission roles through getInfo
      // const hasRoles = store.getters.roles && store.getters.roles.length > 0
      // const hasRoles = sessionStorage.getItem('roles')
      //  console.log(store.getters)
      if (hadLoadRoute && sessionStorage.getItem('roles')) {
        //    console.log('permission.js  ---35')
        next()
      } else {
        //  onsole.log('permission.js  ---38')
        try {
          // get user info
          // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
          // yanyu屏蔽
          // generate accessible routes map based on roles
          const accessRoutes = await store.dispatch('permission/generateRoutes', JSON.parse(sessionStorage.getItem('roles')))
          // dynamically add accessible routes
          router.addRoutes(accessRoutes)
          sessionStorage.setItem('hadLoadRoute', 'true')
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({ ...to, replace: true })
        } catch (error) {
          // remove token and go to login page to re-login
        //  await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
